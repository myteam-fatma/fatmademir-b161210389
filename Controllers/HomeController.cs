﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using MyBlogPage.App_GlobalResources;

namespace MyBlogPage.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public HomeController()
        {
            SetCulture();
        }

        public ActionResult Index()
        {
           
            return View();
        }
        public ActionResult Aboutus()
        {
            return View();
        }

        public ActionResult ChangeCulture(string culture)
        {
            GlobalResource.Culture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            return Redirect("/");
        }

        private void SetCulture()
        {
            var culture = GlobalResource.Culture; ;
            if (culture == null)
                GlobalResource.Culture = new CultureInfo("tr-TR");
        }

    }
}
